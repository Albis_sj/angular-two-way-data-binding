import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; //colocar si o si, para que funcione
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BananaBoxComponent } from './components/banana-box/banana-box.component';

@NgModule({
  declarations: [
    AppComponent,
    BananaBoxComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, //al creae esto, se importa arriba
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
